### Goals
- The trainee will understand what are the differences between different storage media
- The trainee will understand the different storage implementations

### Tasks
- Read about:
  - NVMe
  - PCIe
  - Block Storage
  - Object Storage
- Answer:
  - What are the differences between HDD and SSD?
  - What are the differences between Block Storage, Object Storage and File Storage?
  - Explain the diffrent uscases of each storage type
  - What is the diffrence between bus and port
