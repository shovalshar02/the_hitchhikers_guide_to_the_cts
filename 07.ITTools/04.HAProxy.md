
### Goals
- The trainee will understand how to deploy and manage an HAProxy server

### Tasks
- Read about HAProxy
- Explain what are backends and frontends in HAProxy
- Deploy an HAProxy server, use it to load-balance between 2 different web servers.
- Answer the following questions:
  - How can you change the load-balancing method of HAProxy?
  - How can you use HAProxy to load-balance UDP traffic?
  - What kind of checks can HAProxy can perform?
- Read about load balancers
- Read about nginx, haproxy and traefik
- Read about load balance methods, healthchecks, acls,rate limit, redirect etc...
