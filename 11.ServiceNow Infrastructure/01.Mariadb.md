### Goals
- The trainee will understand the basics of maridb database

### Tasks
- Read about mariadb, mysql and postgres
- Read about maridb replication
- Read about maxscale and galera
- Read about gtid
- Read about inodedb
- Read about mariabackup
- ask you trainer for openshift namespace and create mariadb cluster with master and 2 slaves
- read about duplicity and about kopia
- read about the difference between tar gz and mariabackup