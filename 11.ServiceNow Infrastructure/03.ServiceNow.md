### Goals
- The trainee will understand the basics of servicenow application infrastructure 

### Tasks
- Read about java and servicenow 
- Read about schedulers in servicenow
- Read about glide and orbit in servicenow
- Create servicenow application cluster using servicenow guided deployment guide